'''Exercise 6: Rewrite your pay computation with time-and-a-half for over-
time and create a function called computepay which takes two parameters
(hours and rate).
Enter Hours: 45
Enter Rate: 10
Pay: 475.0'''

try:
    hours = float(input("Enter your hours: "))
    rate_pr_hour = float(input("Enter your rate: "))


    def compute_pay(hours,rate_pr_hour):
        overtime_bonus = 1.5
        usual_hours_limit = 40
        gross_pay = float(hours * rate_pr_hour)
        if hours > usual_hours_limit:
            over_work = float(hours -usual_hours_limit)
            over_work_pay = (over_work * rate_pr_hour*overtime_bonus)+(usual_hours_limit*rate_pr_hour)
            return over_work_pay
        elif hours < 0:
            gross_pay = 0
            return gross_pay

        else:
            return gross_pay

    print(f'pay: {compute_pay(hours,rate_pr_hour)}')
except ValueError:
    print("ERORR! you can only write numeric value")
    






