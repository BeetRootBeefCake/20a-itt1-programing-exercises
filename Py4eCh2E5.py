'''Write a program which prompts the user for a Celsius tem-
perature, convert the temperature to Fahrenheit, and print out the
converted temperature.'''

tempCelsius = float(input("Enter the temperature in celsius: "))
tempFahrenheit = ((tempCelsius*9)/5)+32
print(f'The temperature is {tempFahrenheit} F')





