'''Exercise 7: Rewrite the grade program from the previous chapter using
a function called computegrade that takes a score as its parameter and
returns a grade as a string. '''
try:
    while True:
        score =float(input("Enter your score between 0.0 - 1.0: "))

        def compute_grade(score):
                    if score > 1.0:
                        grade = "Bad"
                        return grade
                    elif score <0.0:
                        grade = "Bad"
                        return
                    elif score >= 0.9:
                        grade = "A"
                        return grade
                    elif score >= 0.8:
                        grade = "B"
                        return grade
                    elif score >= 0.7:
                        grade = "C"
                        return grade
                    elif score >= 0.6:
                        grade = "D"
                        return grade
                    elif score < 0.6:
                        grade = "F"
                        return grade


        print(f'Score:{compute_grade(score)}')

except ValueError:
    print("Error you have to write a numeric value")






