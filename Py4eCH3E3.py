'''Exercise 3: Write a program to prompt for a score between 0.0 and
1.0. If the score is out of range, print an error message. If the score is
between 0.0 and 1.0, print a grade using the following table:'''

try:
    while True:
        Score = float(input("enter your score between 0.0 - 1.0: "))
        if Score > 1.0:
            print("Error: Write a score between 0.0 and 1.0")
        elif Score <0.0:
            print("Error: Write a score between 0.0 - 1.0")
        elif Score >= 0.9:
            print("A")
        elif Score >= 0.8:
            print("B")
        elif Score >= 0.7:
            print("C")
        elif Score >= 0.6:
            print("D")
        elif Score < 0.6:
            print("F")


except ValueError:
        print("Invalid value")

