'''Exercise 2: Rewrite your pay program using try and except so that your
program handles non-numeric input gracefully by printing a message
and exiting the program. The following shows two executions of the
program'''

try:
    Hours = int(input("Enter your hours: "))
    RatePerHour = float(input("Enter your rate per hour: "))
    GrossPay = Hours * RatePerHour
    # Because our standard rate will be the same before 40 hours, there
    if Hours > 40:
        OverWork = Hours - 40
        GrossPay1 = (OverWork * RatePerHour * 1.5) + (40 * RatePerHour)
        print(f'your salary is: {GrossPay1}')

    else:
        print(f'your salary is: {GrossPay}')

except ValueError:
    print("Error: You have to write the numeric value ")


