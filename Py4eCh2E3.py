'''Exercise 3: Write a program to prompt the user for hours and rate per
hour to compute gross pay.'''

#variables
Hours = int(input("Enter your hours: "))
RatePerHour = float(input("Enter your rate per hour: "))
GrossPay = Hours * RatePerHour
#Result of the rate and hours
print(f'pay: {"%.2f"%GrossPay}')
#"%.2f"%GrossPay --->  code converts floats to 2 digits.
# we could also use round() function. as round(GrossPay,2) the 2 is for two digits.




